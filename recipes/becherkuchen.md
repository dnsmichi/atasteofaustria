# Becherkuchen

`Becher` translates to `cup` from Austrian to English. `Becherkuchen = Cup baked cake`.

The idea is to not use any unit measurements, just cups.

The original recipe is from Mühlviertel, Austria and allows to bake a tasty chocolate cake with nuts.

Over the years, I’ve tried variations with different tastes and hidden specials.

## Prepare

- 3 eggs
- 1/2 cup of oil. 
  - Tip: Sunflower.
- 1 cup of sugar (white)
  - A little less, and add vanilla sugar.
- 1 cup of sour cream or yoghurt
  - Take less fat yoghurt.
- 1 cup of these:
  - grated nuts or almonds
  - poppyseed
  - oats
  - marzipan paste
  - Drageekeksi cereals
- 1 cup of flour
  - Tip: Spelt for better taste.
- 1 cup cocoa.
  - Tip: Nesquik chocolate powder. 
- 1/2 baking powder (1 package = 0.56 oz (16g))

For baking:

- Butter to remove the cake easier later
- Round or rectangular baking pan

Heat the oven at 180 degrees Celcius / 356 degrees Fahrenheit. 

## Build

- Mix 3 eggs, oil and sugar. Stir until frothy.
- Stir in yoghurt/sour cream
- Fold in baking powder, flour, cocoa and nuts (or anything listed above)

Grease the baking pan with butter and fill in the mass. 

Bake for 45 minutes. 

Use a fork to stab and when it remains clean, the cake is ready.

## Deploy

Serve fresh from the oven, or later.

Optional toppings:

- Whipped cream
- Fresh fruits
- Powdered sugar

# Original Recipe

![Becherkuchen](../images/becherkuchen_recipe.jpg)
